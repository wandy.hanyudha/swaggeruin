package com.example.swaggeruin.service;

public interface ParsingService {
    Object parse(String url);
}
