package com.example.swaggeruin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "kerjasama")
public class KerjasamaController {

    @Value("${api.key}")
    private String apiKey;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/all")
    public List<Object> getAllKerjaSama(){



        HttpHeaders headers = new HttpHeaders();
        headers.set("x-api-key", apiKey);

        final HttpEntity entity = new HttpEntity<>(headers);

        ResponseEntity<Map> response = restTemplate.exchange(
                "https://apiv2.uinjkt.ac.id/api/kerjasama",
                HttpMethod.GET,
                entity,
                Map.class);

        Map responseBody = response.getBody();
        return Arrays.asList(responseBody);
    }

    @RequestMapping("/test")
    public String getTest(){
        final RestTemplate restTemplate = new RestTemplate();
        final String response = restTemplate.getForObject("https://httpbin.org/ip", String.class);
        return response;
    }

}
